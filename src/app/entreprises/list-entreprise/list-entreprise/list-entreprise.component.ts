import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseService } from 'src/app/services/entreprise/entreprise.service';

@Component({
  selector: 'app-list-entreprise',
  templateUrl: './list-entreprise.component.html',
  styleUrls: ['./list-entreprise.component.css']
})
export class ListEntrepriseComponent implements OnInit {

  entreprises!: Entreprise[];
  selectedEntrepriseId: number;

  constructor(private entrepriseService: EntrepriseService, private toastr: ToastrService) {}

    ngOnInit() {
        this.entrepriseService.getAll()
            .pipe(first())
            .subscribe(entreprises => this.entreprises = entreprises);
    }

    deleteEntreprise(id: number) {
      this.selectedEntrepriseId = id;
    }

    confirmDelete() {
      this.entrepriseService.delete(this.selectedEntrepriseId).subscribe(
          (data) => {
              this.entreprises = this.entreprises.filter(
                  (entreprise) =>
                  entreprise.id !== this.selectedEntrepriseId
              );
              this.toastr.success('Entreprise supprimée avec succès');
          },
          (err) => {}
      );
  }


}
