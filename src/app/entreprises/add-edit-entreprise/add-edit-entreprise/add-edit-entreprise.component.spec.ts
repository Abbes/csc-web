import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEntrepriseComponent } from './add-edit-entreprise.component';

describe('AddEditEntrepriseComponent', () => {
  let component: AddEditEntrepriseComponent;
  let fixture: ComponentFixture<AddEditEntrepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditEntrepriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEntrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
