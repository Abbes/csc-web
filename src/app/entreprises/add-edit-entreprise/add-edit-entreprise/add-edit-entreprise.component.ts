import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { Entreprise } from 'src/app/models/entreprise';
import { Network } from 'src/app/models/network';
import { NetworkFilter } from 'src/app/models/networkFilter';
import { EntrepriseService } from 'src/app/services/entreprise/entreprise.service';
import { NetworkService } from 'src/app/services/network/network.service';


@Component({
  selector: 'app-add-edit-entreprise',
  templateUrl: './add-edit-entreprise.component.html',
  styleUrls: ['./add-edit-entreprise.component.css']
})
export class AddEditEntrepriseComponent implements OnInit {

    addEntrepriseForm!: FormGroup;
    id!: number;
    isAddMode!: boolean;
    loading = false;
    submitted = false;
    entrepriseAdd: Entreprise;
    networks: Network[];
    networkAdd: Network[];
    filtredNetworks: Network[];
    networkFilter: NetworkFilter = new NetworkFilter();

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private entrepriseService: EntrepriseService,
        private networkService: NetworkService,
        private toastr: ToastrService
    ) {}
    
    ngOnInit() {

        this.getNetworks();
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        
        this.addEntrepriseForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            isActive: [true, Validators.required],
            comment: ['', Validators.required],
            networks: new FormArray([])
        });

        if (!this.isAddMode) {
            this.entrepriseService.getById(this.id)
                .pipe(first())
                .subscribe(x => {
                    this.addEntrepriseForm.patchValue(x);
                    this.networkAdd=x.networks;
                    this.patchNetworksValue(x);
                });  
            
        }
    }

    patchNetworksValue(data) {
        let control = <FormArray>this.addEntrepriseForm.controls.networks;
        data.networks.forEach((x) => {
            control.push(this.formBuilder.group(x));
        });
    }

    get f() {
        return this.addEntrepriseForm.controls;
    }

    getNetworks() {
        this.networkService.getAll().subscribe(
            (data) => {
                this.networks = data;
                this.filtredNetworks = this.networks;
            },
            (err) => {}
        );
    }

    onCheckChange(event) {
        let control = <FormArray>this.addEntrepriseForm.controls.networks;

        if (event.target.checked) {
            const findElement = this.networks.find((element) => {
                return element.id === parseInt(event.target.value);
            });
            control.push(this.formBuilder.group(findElement));
        } else {
            const findIndexElement = control.value.findIndex((element) => {
                return parseInt(element.id) === parseInt(event.target.value);
            });
            control.removeAt(findIndexElement);
        }
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.addEntrepriseForm.invalid) {
            return this.toastr.error("error");
        }
        this.loading = true;
        if (this.isAddMode) {
            this.createEntreprise();
        } else {
            this.updateEntreprise();
        }
    }

    private createEntreprise() {
        this.entrepriseService.create(this.addEntrepriseForm.value)
            .pipe(first())
            .subscribe(() => {                
                this.router.navigate(['../'], { relativeTo: this.route });
            })
            .add(() => this.loading = false);
    }

    private updateEntreprise() {
        this.entrepriseService.update(this.id, this.addEntrepriseForm.value)
            .pipe(first())
            .subscribe(() => {               
                this.router.navigate(['../../'], { relativeTo: this.route });
            })
            .add(() => this.loading = false);
    }

    checkIfChecked(value: number) {
        let control = <FormArray>this.addEntrepriseForm.controls.networks;
        if (control) {
            const exist = control.value.filter(
                (network) => network.id == value
            );
            return exist !== undefined && exist.length > 0;
        }
    }

    filter(searchTerm) {
        this.filtredNetworks = this.networks.filter((it) => {
            return it.name.toLowerCase().includes(searchTerm);
        });
    }
    
}
