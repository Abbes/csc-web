import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Entreprise } from 'src/app/models/entreprise';


const baseUrl = `${environment.apiUrl}/entreprises`;

@Injectable({ providedIn: 'root' })
export class EntrepriseService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Entreprise[]>(baseUrl);
    }

    getById(id: number) {
        return this.http.get<Entreprise>(`${baseUrl}/${id}`);
    }

    create(params: any) {
        return this.http.post(baseUrl, params);
    }

    update(id: number, params: any) {
        return this.http.put(`${baseUrl}/${id}`, params);
    }

    delete(id: number) {
        return this.http.delete(`${baseUrl}/${id}`);
    }
}