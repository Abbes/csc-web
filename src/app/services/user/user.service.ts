import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  createUser(data){
    return this.http.post<User>(`${environment.apiUrl}/users`, data);
  }

  getById(id: number) {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }

  updateUser(id, data){
    return this.http.put<User>(`${environment.apiUrl}/users/${id}`, data);
  }

  deleteById(id: number){
    return this.http.delete(`${environment.apiUrl}/users/${id}`);
  }
}
