import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Application } from 'src/app/models/application';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get<Application>(`${environment.apiUrl}/applications/${id}`);
  }

  getAll(){
    return this.http.get<Application[]>(`${environment.apiUrl}/applications`);
  }

  deleteById(id: number){
    return this.http.delete(`${environment.apiUrl}/applications/${id}`);
  }

  createApplication(data){
    return this.http.post<Application>(`${environment.apiUrl}/applications`, data);
  }

  updateApplication(id, data){
    return this.http.put<Application>(`${environment.apiUrl}/applications/${id}`, data);
  }
}
