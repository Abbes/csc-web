import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from 'src/app/models/network';
import { environment } from 'src/environments/environment';

const baseUrl = `${environment.apiUrl}/networks`;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get<Network[]>(`${environment.apiUrl}/networks`);
  }

    getById(id: number) {
        return this.http.get<Network>(`${baseUrl}/${id}`);
    }

    create(params: any) {
        return this.http.post(baseUrl, params);
    }

    update(id: number, params: any) {
        return this.http.put(`${baseUrl}/${id}`, params);
    }

    delete(id: number) {
        return this.http.delete(`${baseUrl}/${id}`);
    }
}
