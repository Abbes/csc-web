import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user/user.service";

@Component({
    selector: "app-users-list",
    templateUrl: "./users-list.component.html",
    styleUrls: ["./users-list.component.css"],
})
export class UsersListComponent implements OnInit {
    users: User[];
    selectedUserId: number;
    userDetails: User;

    constructor(
        private userService: UserService,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.getUsers();
    }

    getUsers(): void {
        this.userService.getAll().subscribe(
            (data) => {
                this.users = data;
            },
            (err) => {}
        );
    }

    deleteUser(id: number): void {
        this.selectedUserId = id;
    }

    confirmDelete() {
        this.userService.deleteById(this.selectedUserId).subscribe(
            (data) => {
                this.users = this.users.filter(
                    (application) => application.id !== this.selectedUserId
                );
                this.toastr.success("Administrateur supprimé avec succès");
            },
            (err) => {}
        );
    }
}