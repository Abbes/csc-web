import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user/user.service";

@Component({
    selector: "app-add-edit-user",
    templateUrl: "./add-edit-user.component.html",
    styleUrls: ["./add-edit-user.component.css"],
})
export class AddEditUserComponent implements OnInit {
    addUserForm: FormGroup;
    isAddMode: boolean;
    selectedUserId: number;
    submitted = false;
    user: User;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.selectedUserId = this.route.snapshot.params["id"];
        this.isAddMode = !this.selectedUserId;
        this.buildForm();

        if (!this.isAddMode) {
            this.userService.getById(this.selectedUserId).subscribe(
                (data) => {
                    this.addUserForm.patchValue(data);
                    this.user = data;
                },
                (err) => {
                    if (err) {
                        this.router.navigate(["/users"]);
                    }
                }
            );
        }
    }

    /**
     * Init Form with validation
     */
    buildForm(): void {
        this.addUserForm = this.formBuilder.group({
            identifier: ["", Validators.required],
            email: ["", [Validators.required, Validators.email]],
            firstName: ["", Validators.required],
            lastName: ["", Validators.required],
            isActive: [true, Validators.required],
            role: ["Administrateur", Validators.required],
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.addUserForm.invalid) {
            return;
        }

        if (!this.isAddMode) {
            this.updateUser();
        } else {
            this.createUser();
        }
    }

    /**
     * Get formValidation errors
     */
    get f() {
        return this.addUserForm.controls;
    }

    createUser() {
        this.userService.createUser(this.addUserForm.value).subscribe(
            (data) => {
                this.router.navigate(["/users"]).then(() => {
                    this.toastr.success("Administrateur ajouté avec succès");
                });
            },
            (err) => {}
        );
    }

    updateUser() {
        this.userService
            .updateUser(this.selectedUserId, this.addUserForm.value)
            .subscribe(
                (data) => {
                    this.router.navigate(["/users"]).then(() => {
                        this.toastr.success(
                            "Administrateur modifié avec succès"
                        );
                    });
                },
                (err) => {}
            );
    }
}