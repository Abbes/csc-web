import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { Application } from 'src/app/models/application';
import { ApplicationFilter } from 'src/app/models/applicationFilter';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseFilter } from 'src/app/models/entrepriseFilter';
import { Network } from 'src/app/models/network';
import { NetworkFilter } from 'src/app/models/networkFilter';
import { ApplicationService } from 'src/app/services/application/application.service';
import { EntrepriseService } from 'src/app/services/entreprise/entreprise.service';
import { NetworkService } from 'src/app/services/network/network.service';

@Component({
  selector: 'app-add-edit-network',
  templateUrl: './add-edit-network.component.html',
  styleUrls: ['./add-edit-network.component.css']
})
export class AddEditNetworkComponent implements OnInit {

  addNetworkForm!: FormGroup;
  id!: number;
  isAddMode!: boolean;
  loading = false;
  submitted = false;
  applications: Application[];
  entreprises: Entreprise[];
  networks: Network[];
  filtredEntreprises: Entreprise[];
  filtredApplications: Application[];
 
  networkFilter: NetworkFilter = new NetworkFilter();
  applicationFilter: ApplicationFilter = new ApplicationFilter();
  entrepriseFilter: EntrepriseFilter = new EntrepriseFilter();
   

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private entrepriseService: EntrepriseService,
      private applicationService: ApplicationService,
      private networkService: NetworkService,
      private toastr: ToastrService
  ) {}
  
  ngOnInit() {

    this.getApplication();
      this.getEntreprise();
      
      this.id = this.route.snapshot.params['id'];
      this.isAddMode = !this.id;

      
      this.addNetworkForm = this.formBuilder.group({
          name: ['', Validators.required],
          description: ['', [Validators.required]],
          isActive: [true, Validators.required],
          applications: new FormArray([]),
          entreprises: new FormArray([])

      });
      if (!this.isAddMode) {
        this.networkService.getById(this.id)
            .pipe(first())
            .subscribe(x => {
                this.addNetworkForm.patchValue(x);
                this.patchApplicationsValue(x);
                this.patchEntreprisesValue(x);
            });  
    }
    
  }

  patchApplicationsValue(data) {
    let control = <FormArray>this.addNetworkForm.controls.applications;
    data.applications.forEach((x) => {
        control.push(this.formBuilder.group(x));
    });
}
patchEntreprisesValue(data) {
    let control = <FormArray>this.addNetworkForm.controls.entreprises;
    data.entreprises.forEach((x) => {
        control.push(this.formBuilder.group(x));
    });
}
  getEntreprise() {
    this.entrepriseService.getAll()
    .pipe(first())
    .subscribe(entreprises => this.entreprises = entreprises);

    this.entrepriseService.getAll().subscribe(
        (data) => {
            this.entreprises = data;
            this.filtredEntreprises = data;
        },
        (err) => {}
    );
}
getApplication() {

    this.applicationService.getAll().subscribe(
        (data) => {
            this.applications = data;
            this.filtredApplications = this.applications;
        },
        (err) => {}
    );
}

  get f() {
      return this.addNetworkForm.controls;
  }

  checkIfChecked(value: number) {
    let control = <FormArray>this.addNetworkForm.controls.entreprises;
    if (control) {
        const exist = control.value.filter(
            (entreprises) => entreprises.id == value
        );
        return exist !== undefined && exist.length > 0;
    }
}

checkIfCheckedApplication(value: number) {
    let control = <FormArray>this.addNetworkForm.controls.applications;
    if (control) {
        const exist = control.value.filter(
            (applications) => applications.id == value
        );
        return exist !== undefined && exist.length > 0;
    }
}

  onCheckChange(event) {
      let control = <FormArray>this.addNetworkForm.controls.entreprises;

      if (event.target.checked) {
          const findElement = this.entreprises.find((element) => {
              return element.id === parseInt(event.target.value);
          });
          control.push(this.formBuilder.group(findElement));
      } else {
          const findIndexElement = control.value.findIndex((element) => {
              return parseInt(element.id) === parseInt(event.target.value);
          });
          control.removeAt(findIndexElement);
      }
  }

  onCheckChangeApplication(event) {
    let control = <FormArray>this.addNetworkForm.controls.applications;

    if (event.target.checked) {
        const findElement = this.applications.find((element) => {
            return element.id === parseInt(event.target.value);
        });
        control.push(this.formBuilder.group(findElement));
    } else {
        const findIndexElement = control.value.findIndex((element) => {
            return parseInt(element.id) === parseInt(event.target.value);
        });
        control.removeAt(findIndexElement);
    }
}


  onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.addNetworkForm.invalid) {
          return this.toastr.error("error");
      }
      this.loading = true;
      if (this.isAddMode) {
          this.createNetwork();
      } else {
          this.updateNetwork();
      }
  }

  private createNetwork() {
      this.networkService.create(this.addNetworkForm.value)
          .pipe(first())
          .subscribe(() => {                
              this.router.navigate(['../'], { relativeTo: this.route });
          })
          .add(() => this.loading = false);
  }

  private updateNetwork() {
      this.networkService.update(this.id, this.addNetworkForm.value)
          .pipe(first())
          .subscribe(() => {               
              this.router.navigate(['../../'], { relativeTo: this.route });
          })
          .add(() => this.loading = false);
  }

  filterEntreprise(searchTerm) {
    this.filtredEntreprises = this.entreprises.filter((it) => {
        return it.name.toLowerCase().includes(searchTerm);
    });
}
filterApplication(searchTerm) {
    this.filtredApplications = this.applications.filter((it) => {
        return it.name.toLowerCase().includes(searchTerm);
    });
}

}
