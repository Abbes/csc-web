import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditNetworkComponent } from './add-edit-network.component';

describe('AddEditNetworkComponent', () => {
  let component: AddEditNetworkComponent;
  let fixture: ComponentFixture<AddEditNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditNetworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
