import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { Network } from 'src/app/models/network';
import { NetworkService } from 'src/app/services/network/network.service';

@Component({
  selector: 'app-list-network',
  templateUrl: './list-network.component.html',
  styleUrls: ['./list-network.component.css']
})
export class ListNetworkComponent implements OnInit {

  networks!: Network[];
  selectedNetworkId: number;

  constructor(private networkService: NetworkService, private toastr: ToastrService) {}

    ngOnInit() {
        this.networkService.getAll()
            .pipe(first())
            .subscribe(networks => this.networks = networks);
    }

    deleteNetwork(id: number) {
      this.selectedNetworkId = id;
    }

    confirmDelete() {
      this.networkService.delete(this.selectedNetworkId).subscribe(
          (data) => {
              this.networks = this.networks.filter(
                  (network) =>
                  network.id !== this.selectedNetworkId
              );
              this.toastr.success('Réseau supprimé avec succès');
          },
          (err) => {}
      );
  }

}
