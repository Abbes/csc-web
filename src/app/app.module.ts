import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddEditApplicationComponent } from './applications/add-edit-application/add-edit-application.component';
import { ApplicationsListComponent } from './applications/applications-list/applications-list.component';
import { AddEditUserComponent } from './users/add-edit-user/add-edit-user.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddEditEntrepriseComponent } from './entreprises/add-edit-entreprise/add-edit-entreprise/add-edit-entreprise.component';
import { ListEntrepriseComponent } from './entreprises/list-entreprise/list-entreprise/list-entreprise.component';
import { AddEditNetworkComponent } from './networks/add-edit-network/add-edit-network.component';
import { ListNetworkComponent } from './networks/list-network/list-network.component';


@NgModule({
  declarations: [
    AppComponent,
    AddEditApplicationComponent,
    ApplicationsListComponent,
    AddEditUserComponent,
    UsersListComponent,    
    AddEditEntrepriseComponent,
    ListEntrepriseComponent,
    AddEditNetworkComponent,
    ListNetworkComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
