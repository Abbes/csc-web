import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddEditApplicationComponent } from "./applications/add-edit-application/add-edit-application.component";
import { ApplicationsListComponent } from "./applications/applications-list/applications-list.component";
import { AddEditUserComponent } from "./users/add-edit-user/add-edit-user.component";
import { UsersListComponent } from "./users/users-list/users-list.component";
import { AddEditEntrepriseComponent } from "./entreprises/add-edit-entreprise/add-edit-entreprise/add-edit-entreprise.component";
import { ListEntrepriseComponent } from "./entreprises/list-entreprise/list-entreprise/list-entreprise.component";
import { ListNetworkComponent } from "./networks/list-network/list-network.component";
import { AddEditNetworkComponent } from "./networks/add-edit-network/add-edit-network.component";

const routes: Routes = [
  { path: "networks", component: ListNetworkComponent },
  { path: "networks/add", component: AddEditNetworkComponent },
  { path: "networks/edit/:id", component: AddEditNetworkComponent },
  { path: "entreprises", component: ListEntrepriseComponent },
  { path: "entreprises/add", component: AddEditEntrepriseComponent },
  { path: "entreprises/edit/:id", component: AddEditEntrepriseComponent },
  { path: "applications/edit/:id", component: AddEditApplicationComponent },
  { path: "applications", component: ApplicationsListComponent },
  { path: "new-application", component: AddEditApplicationComponent },
  { path: "users/edit/:id", component: AddEditUserComponent },
  { path: "new-user", component: AddEditUserComponent },
  { path: "users", component: UsersListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
