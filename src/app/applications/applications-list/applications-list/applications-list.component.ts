import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Application } from "src/app/models/application";
import { ApplicationService } from "src/app/services/application/application.service";

@Component({
    selector: "app-applications-list",
    templateUrl: "./applications-list.component.html",
    styleUrls: ["./applications-list.component.css"],
})
export class ApplicationsListComponent implements OnInit {
    applications: Application[];
    selectedApplicationId: number;
    constructor(private applicationSerice: ApplicationService, private toastr: ToastrService) {}

    ngOnInit(): void {
        this.getApplications();
    }

    getApplications(): void {
        this.applicationSerice.getAll().subscribe(
            (data) => {
                this.applications = data;
            },
            (err) => {}
        );
    }

    deleteApplication(id: number) {
        this.selectedApplicationId = id;
    }

    confirmDelete() {
        this.applicationSerice.deleteById(this.selectedApplicationId).subscribe(
            (data) => {
                this.applications = this.applications.filter(
                    (application) =>
                        application.id !== this.selectedApplicationId
                );
                this.toastr.success('Application supprimée avec succès');
            },
            (err) => {}
        );
    }
}
