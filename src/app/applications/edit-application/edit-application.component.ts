import { Component, OnInit } from "@angular/core";
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Application } from "src/app/models/application";
import { Network } from "src/app/models/network";
import { NetworkFilter } from "src/app/models/networkFilter";
import { ApplicationService } from "src/app/services/application/application.service";
import { NetworkService } from "src/app/services/network/network.service";

@Component({
    selector: "app-edit-application",
    templateUrl: "./edit-application.component.html",
    styleUrls: ["./edit-application.component.css"],
})
export class EditApplicationComponent implements OnInit {
    addApplicationForm: FormGroup;
    loading = false;
    submitted = false;
    application: Application;
    filtredNetworks: Network[];
    networkFilter: NetworkFilter = new NetworkFilter();
    networks: Network[];
    selectedApplicationId: number;

    constructor(
        private formBuilder: FormBuilder,
        private applicationService: ApplicationService,
        private networkService: NetworkService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.selectedApplicationId = this.route.snapshot.params["id"];
        this.buildForm();
        this.customValidationForm();
        this.customValidateUrl();
        this.getNetworks();
        this.applicationService.getById(this.selectedApplicationId).subscribe(
            (data) => {
                this.addApplicationForm.patchValue(data);
                this.application = data;
                this.patchNetworksValue(data);
            },
            (err) => {
                if (err) {
                    this.router.navigate(["/applications"]);
                }
            }
        );
    }

    getNetworks() {
        this.networkService.getAll().subscribe(
            (data) => {
                this.filtredNetworks = data;
                this.networks = this.filtredNetworks;
            },
            (err) => {}
        );
    }

    patchNetworksValue(data) {
        let control = <FormArray>this.addApplicationForm.controls.networks;
        data.networks.forEach((x) => {
            control.push(this.formBuilder.group(x));
        });
    }

    /**
     * Init Form with validation
     */
    buildForm(): void {
        const regEx = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
        this.addApplicationForm = this.formBuilder.group({
            name: ["", Validators.required],
            version: ["", Validators.required],
            protocol: ["", Validators.required],
            auth: ["", null],
            isActive: [true, Validators.required],
            url: ["", Validators.pattern(regEx)],
            description: ["", null],
            jsFunctions: ["", null],
            networks: new FormArray([]),
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.addApplicationForm.invalid) {
            return;
        }
        this.applicationService
            .updateApplication(
                this.selectedApplicationId,
                this.addApplicationForm.value
            )
            .subscribe(
                (data) => {
                    this.router.navigate(["/applications"]).then(() => {
                      this.toastr.success('Application modifiée avec succès');
                    })
                },
                (err) => {
                    console.log("error", err);
                }
            );
    }

    customValidationForm(): void {
        const authInput = this.addApplicationForm.get("auth");
        this.addApplicationForm
            .get("protocol")
            .valueChanges.subscribe((protocol) => {
                console.log(protocol);
                if (protocol === "form") {
                    authInput.setValidators([Validators.required]);
                } else {
                    authInput.setValidators(null);
                }
                authInput.updateValueAndValidity();
            });
    }

    customValidateUrl(): void {
        const networkInputs = this.addApplicationForm.get("networks");
        const urlInput = this.addApplicationForm.get("url");
        this.addApplicationForm
            .get("networks")
            .valueChanges.subscribe((network) => {
                if (networkInputs.value.length > 0) {
                    urlInput.setValidators([Validators.required]);
                } else {
                    urlInput.setValidators(null);
                }
                urlInput.updateValueAndValidity();
            });
    }

    onCheckChange(event) {
        let control = <FormArray>this.addApplicationForm.controls.networks;

        if (event.target.checked) {
            const findElement = this.networks.find((element) => {
                return element.id === parseInt(event.target.value);
            });
            control.push(this.formBuilder.group(findElement));
        } else {
            const findIndexElement = control.value.findIndex((element) => {
                return parseInt(element.id) === parseInt(event.target.value);
            });
            control.removeAt(findIndexElement);
        }
    }

    checkIfChecked(value: number) {
        let control = <FormArray>this.addApplicationForm.controls.networks;
        if (control) {
            const exist = control.value.filter(
                (network) => network.id == value
            );
            return exist !== undefined && exist.length > 0;
        }
    }

    filter(searchTerm) {
        this.filtredNetworks = this.networks.filter((it) => {
            return it.name.toLowerCase().includes(searchTerm);
        });
    }

    get f() {
        return this.addApplicationForm.controls;
    }
}
