import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Application } from "src/app/models/application";
import { Network } from "src/app/models/network";
import { NetworkFilter } from "src/app/models/networkFilter";
import { ApplicationService } from "src/app/services/application/application.service";
import { NetworkService } from "src/app/services/network/network.service";

@Component({
    selector: "app-add-edit-application",
    templateUrl: "./add-edit-application.component.html",
    styleUrls: ["./add-edit-application.component.css"],
})
export class AddEditApplicationComponent implements OnInit {
    addApplicationForm: FormGroup;
    loading = false;
    submitted = false;
    urlValidatorRegex = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
    networks: Network[];
    filtredNetworks: Network[];
    networkFilter: NetworkFilter = new NetworkFilter();
    selectedApplicationId: number;
    application: Application;
    isAddMode: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private applicationService: ApplicationService,
        private router: Router,
        private route: ActivatedRoute,
        private networkService: NetworkService,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.selectedApplicationId = this.route.snapshot.params['id'];
        this.isAddMode = !this.selectedApplicationId;
        this.buildForm();
        this.getNetworks();
        this.customValidationForm();
        this.customValidateUrl();

        if (!this.isAddMode) {
            this.applicationService
                .getById(this.selectedApplicationId)
                .subscribe(
                    (data) => {
                        this.addApplicationForm.patchValue(data);
                        this.application = data;
                        this.patchNetworksValue(data);
                    },
                    (err) => {
                        if (err) {
                            this.router.navigate(["/applications"]);
                        }
                    }
                );
        }
    }

    /**
     * Append values of networks on form
     * @param data 
     */
    patchNetworksValue(data) {
        let control = <FormArray>this.addApplicationForm.controls.networks;
        data.networks.forEach((x) => {
            control.push(this.formBuilder.group(x));
        });
    }

    /**
     * Get All networks
     */
    getNetworks(): void {
        this.networkService.getAll().subscribe(
            (data) => {
                this.networks = data;
                this.filtredNetworks = this.networks;
            },
            (err) => {}
        );
    }

    /**
     * Init Form with validation
     */
    buildForm(): void {
        this.addApplicationForm = this.formBuilder.group({
            name: ["", Validators.required],
            version: ["", Validators.required],
            protocol: ["", Validators.required],
            auth: ["", null],
            isActive: [true, Validators.required],
            url: ["", Validators.pattern(this.urlValidatorRegex)],
            description: ["", null],
            jsFunctions: ["", null],
            networks: new FormArray([]),
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.addApplicationForm.invalid) {
            return;
        }

        if (!this.isAddMode) {
            this.updateApplication();
        } else {
            this.createApplication();
        }
    }

    createApplication(): void {
        this.applicationService
            .createApplication(this.addApplicationForm.value)
            .subscribe(
                (data) => {
                    this.router.navigate(["/applications"]).then(() => {
                        this.toastr.success("Application ajoutée avec succès");
                    });
                },
                (err) => {}
            );
    }

    updateApplication(): void {
        this.applicationService
            .updateApplication(
                this.selectedApplicationId,
                this.addApplicationForm.value
            )
            .subscribe(
                (data) => {
                    this.router.navigate(["/applications"]).then(() => {
                        this.toastr.success("Application modifiée avec succès");
                    });
                },
                (err) => {
                }
            );
    }

    customValidationForm(): void {
        const authInput = this.addApplicationForm.get("auth");
        this.addApplicationForm
            .get("protocol")
            .valueChanges.subscribe((protocol) => {
                if (protocol === "form") {
                    authInput.setValidators([Validators.required]);
                } else {
                    authInput.setValidators(null);
                }
                authInput.updateValueAndValidity();
            });
    }

    customValidateUrl(): void {
        const networkInputs = this.addApplicationForm.get("networks");
        const urlInput = this.addApplicationForm.get("url");
        this.addApplicationForm
            .get("networks")
            .valueChanges.subscribe((network) => {
                if (networkInputs.value.length > 0) {
                    urlInput.setValidators([
                        Validators.required,
                        Validators.pattern(this.urlValidatorRegex),
                    ]);
                } else {
                    urlInput.setValidators([
                        Validators.pattern(this.urlValidatorRegex),
                    ]);
                }
                urlInput.updateValueAndValidity();
            });
    }

    onCheckChange(event) {
        let control = <FormArray>this.addApplicationForm.controls.networks;

        if (event.target.checked) {
            const findElement = this.networks.find((element) => {
                return element.id === parseInt(event.target.value);
            });
            control.push(this.formBuilder.group(findElement));
        } else {
            const findIndexElement = control.value.findIndex((element) => {
                return parseInt(element.id) === parseInt(event.target.value);
            });
            control.removeAt(findIndexElement);
        }
    }

    /**
     * Get formValidation errors
     */
    get f() {
        return this.addApplicationForm.controls;
    }

    /**
     * Check if network checkbox is checked
     * @param value 
     */
    checkIfChecked(value: number) {
        let control = <FormArray>this.addApplicationForm.controls.networks;
        if (control) {
            const exist = control.value.filter(
                (network) => network.id == value
            );
            return exist !== undefined && exist.length > 0;
        }
    }

    /**
     * Filter networks by name or company
     * @param searchTerm 
     */
    filterNetwork(searchTerm) {
        this.filtredNetworks = this.networks.filter((it) => {
            return it.name.toLowerCase().includes(searchTerm);
        });
    }
}
