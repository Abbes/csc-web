export class User {
    id!: number;
    identifier: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    isActive: boolean;
}