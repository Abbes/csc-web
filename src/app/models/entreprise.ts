import { Network } from "./network";


export class Entreprise {

    id: number;
    name: string;
    email: string;
    isActive: boolean;
    comment: string;
    networks: Network[];
    


    constructor(id?:number, name?: string, email?: string, isActive?: boolean, comment?: string, networks?: Network[]){
        this.id = id;
        this.name = name;
        this.email = email;
        this.isActive = isActive;
        this.comment = comment;
        this.networks = networks;
        
    }
}
