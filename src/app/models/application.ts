import { Network } from "./network";

export class Application {
    id!: number;
    name: string;
    version: string;
    protocol: string;
    auth: string;
    url: string;
    description: string;
    jsFunctions: string;
    isActive: boolean;
    networks: Network[];
}